#pragma once
#include "Game.h"
#include "Leaderboard.h"
#include <string>

class Database :
	public Game
{
public:
	const char* LeaderboardFilename = "leaderboard.dat";

protected:
	// Game Overrites
	virtual void Init() override;
	virtual void Shutdown() override;
	virtual bool IsGameRunning() override;

	virtual void Update() override;
	virtual void Draw() override;

private:
	bool isGameRunning = true;

	Leaderboard leaderboard;

	void displayMenu();
	std::string getMenuOption();
	void addNewPlayer();
	void modifyPlayerByIndex();
	void modifyPlayerByName();
	void hackLeaderboardFile();
	
};

