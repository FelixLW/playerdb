#include "Leaderboard.h"
#include <fstream>
#include <iostream>
#include <algorithm>

Leaderboard::Leaderboard(unsigned int _maxPlayers)
	: maxPlayers(_maxPlayers), playersInUse(0)
{
	if (maxPlayers == 0)
		maxPlayers = 10;

	if (maxPlayers > 100) {
		maxPlayers = 100;
	}

	playerList = new Player[maxPlayers];
}

Leaderboard::~Leaderboard()
{
	delete[] playerList;
}

void Leaderboard::Draw()
{
	std::cout << " ______________________" << std::endl;
	std::cout << "|                      |" << std::endl;
	std::cout << "|---  Leaderboard   ---|" << std::endl;
	std::cout << "|______________________|" << std::endl;
	std::cout << "" << std::endl;

	if (!IsEmpty())
	{
		for (unsigned int i = 0; i < playersInUse; i++)
		{
			std::cout << " [" << i << "] ";
			playerList[i].Draw();
		}
	}
	else
	{
		std::cout << "...Wow, such empty..." << std::endl;
	}	
}

void Leaderboard::AddPlayer(const std::string& name, unsigned int score)
{
	AddPlayer(Player(name.c_str(), score));
}

void Leaderboard::AddPlayer(const Player& player)
{
	if (playersInUse < maxPlayers)
	{
		playerList[playersInUse] = player;
		playersInUse++;
	}
	else
	{
		throw std::exception("Out of bounds. leaderboard full.");
	}
}

void Leaderboard::Clear()
{
	playersInUse = 0;
}

bool comparePlayerScores(const Player& lhs, const Player& rhs)
{
	return lhs.GetHighScore() > rhs.GetHighScore();
}

bool comparePlayerNames(const Player& lhs, const Player& rhs)
{
	return (strcmp(lhs.GetName(), rhs.GetName()) < 0);
}

void Leaderboard::SortByHighscore()
{
	std::sort(playerList, playerList + playersInUse, comparePlayerScores);
}

void Leaderboard::SortByName()
{
	std::sort(playerList, playerList + playersInUse, comparePlayerNames);

}

bool Leaderboard::BinarySearch(const std::string& name, unsigned int& posFound)
{
	// Need to sort collection before binary search
	SortByName();

	unsigned int l = 0;
	unsigned int r = playersInUse - 1;
	unsigned int m;

	while (l <= r) 
	{
		m = (l + r) / 2;

		if (name == playerList[m].GetName())
		{
			posFound = m;
			return true;
		}

		else if (name < playerList[m].GetName())
		{
			r = m - 1;
		}

		else if (name > playerList[m].GetName())
		{
			l = m + 1;
		}
	}
	
	return false;
}

Player& Leaderboard::operator[](unsigned int pos) const
{
	return GetPlayer(pos);
}

Player& Leaderboard::GetPlayer(unsigned int pos)const
{
	if (pos > playersInUse)
	{
		throw std::out_of_range("Out of bounds.");
	}

	return playerList[pos];
}

bool Leaderboard::Load(const char* filename)
{
	std::ifstream fin(filename, std::ios_base::in | std::ios_base::binary);	
	if (fin.good())
	{
		// Read the max players, players in use, array of players
		unsigned int maxPlayers;
		fin.read((char*)&maxPlayers, sizeof(unsigned int));

		if (maxPlayers > MaxLeaderboardSize)
		{
			std::cerr << "Invalid file format. maxPlayers is too large" << std::endl;
			fin.close();
			return false;
			//throw std::exception("Invalid file data");
		}

		// Reallocate playerList if different size from file
		if (this->maxPlayers != maxPlayers)
		{
			this->maxPlayers = maxPlayers;
			delete[] this->playerList;
			this->playerList = new Player[maxPlayers];
		}

		// Read the players in use
		unsigned int playersInUse;
		fin.read((char*)&playersInUse, sizeof(unsigned int));

		if (playersInUse > maxPlayers)
		{
			std::cerr << "Invalid file format. playersInUse is too large" << std::endl;
			fin.close();
			return false;
		}

		this->playersInUse = playersInUse;

		// Read the array of players
		fin.read((char*)playerList, (std::streamsize)playersInUse * sizeof(Player));
		fin.close();
		return true;
	}
	
	return false;
}

bool Leaderboard::Save(const char* filename)
{
	std::ofstream fout(filename, std::ios_base::out | std::ios_base::binary);

	if (fout.good())
	{
		// Write out maxPlayers to file
		fout.write((const char*)&maxPlayers, sizeof(maxPlayers));

		// Write the playersInUse to file
		fout.write((const char*)&playersInUse, sizeof(playersInUse));

		// Write out the playerList[playersInUse]
		fout.write((const char*)playerList, (std::streamsize)playersInUse * sizeof(Player));

		fout.close();
		return true;
	}

	return false;
}