#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "Database.h"
#include "iostreamutils.h"

void Database::Init()
{
	// Load leaderboard from file
	try
	{
		leaderboard.Load(LeaderboardFilename);
		leaderboard.SortByHighscore();
	}
	catch (std::exception & err)
	{
		std::cout << "Error: " << err.what() << std::endl;
	}	
}

void Database::Shutdown()
{
	// Save leaderboard to file
	std::cout << "Do you want to save leaderboard on exit? (y/n)? > ";
	cinclear();
	unsigned int c;
	std::cin >> c;

	if (c == 'y' || c == 'Y')
	{
		leaderboard.Save(LeaderboardFilename);
	}
}

bool Database::IsGameRunning()
{
	return isGameRunning;
}

void Database::Update()
{
	displayMenu();
	std::string menuOption = getMenuOption();

	if (menuOption == "a")
	{
		addNewPlayer();
	}
	else if (menuOption == "m")
	{
		modifyPlayerByName();
	}
	else if (menuOption == "n")
	{
		leaderboard.SortByName();
	}
	else if (menuOption == "l")
	{
		if (!leaderboard.Load(LeaderboardFilename))
		{
			std::cerr << "Error loading file: " << LeaderboardFilename << std::endl;
			cinclear();
			int ch = getchar();
		}
		else 
		{
			leaderboard.SortByHighscore();
		}		
	}
	else if (menuOption == "s")
	{
		if (!leaderboard.Save(LeaderboardFilename))
		{
			std::cerr << "Error saving file: " << LeaderboardFilename << std::endl;
			cinclear();
			int ch = getchar();
		}
	}
	else if (menuOption == "h")
	{
		hackLeaderboardFile();
	}
	else if (menuOption == "c") 
	{
		leaderboard.Clear();
	}
	else if (menuOption == "q")
	{
		isGameRunning = false;
	}
}

void Database::Draw()
{
	system("cls");
	leaderboard.Draw();
}

void Database::displayMenu()
{
	std::cout << std::endl << "--- Menu ---" << std::endl;
	std::cout << "(A)dd player" << std::endl;
	std::cout << "(M)odify player" << std::endl;
	std::cout << "Sort by (N)ame" << std::endl;
	std::cout << "(C)lear leaderboard" << std::endl;
	std::cout << "(L)oad leaderboard" << std::endl;
	std::cout << "(S)ave leaderboard" << std::endl;
	std::cout << "(Q)uit" << std::endl;
	std::cout << std::endl << "------------" << std::endl;
	std::cout << "> ";
}

std::string Database::getMenuOption()
{
	std::string userInput;
	cinclear(); // Clear pending input
	std::cin >> userInput;

	std::transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower);

	return userInput;
}

void Database::addNewPlayer()
{
	if (!leaderboard.IsFull())
	{
		Player p;
		if (p.LoadFromConsole())
		{
			leaderboard.AddPlayer(p);
			leaderboard.SortByHighscore();
		}
	}
	else
	{
		std::cout << "Leaderboard is full" << std::endl;
	}
}

void Database::modifyPlayerByIndex()
{
	cinclear();
	std::cout << "Enter player position to modify: ";
	unsigned int pos;
	std::cin >> pos;

	if (pos < leaderboard.PlayersInUse())
	{
		leaderboard[pos].LoadFromConsole();
		leaderboard.SortByHighscore();
	}
}

void Database::modifyPlayerByName()
{
	cinclear();
	std::cout << "Enter name of player to modify: ";
	std::string name;
	std::cin >> name;

	// Ask the leaderoard to do a binary search for the player and return the players position in the array
	unsigned int pos = 0;

	if (leaderboard.BinarySearch(name, pos))
	{
		leaderboard[pos].LoadFromConsole();
		leaderboard.SortByHighscore();
	}
}

void Database::hackLeaderboardFile()
{
	cinclear();

	std::cout << "Enter name of player to modify: ";
	std::string name;
	std::cin >> name;

	cinclear();

	std::fstream f(LeaderboardFilename, std::ios_base::in | std::ios_base::out | std::ios_base::binary);
	if (f.good())
	{
		// Read past the header info (maxPlayers, playersInUse)
		unsigned int maxPlayers, playersInUse;

		f.read((char*)&maxPlayers, sizeof(maxPlayers));
		f.read((char*)&playersInUse, sizeof(playersInUse));

		//f.seekg(sizeof(unsigned int) * 2);

		// 
		Player p;
		while (!f.eof())
		{
			f.read((char*)&p, sizeof(Player));
			if (name == p.GetName())
			{
				std::cout << "Found player: " << name << std::endl;

				// Read new player data from console
				p.LoadFromConsole();

				// Move (seek) the cursor back to the start of this player in the file
				const int sizeOfPlayer = sizeof(Player);
				f.seekg(-sizeOfPlayer, f.cur);

				// Overwrite the file data with the new player data
				f.write((const char*)&p, sizeof(Player));

				f.close();

				int ch = getchar();
				return;
			}
		}

		std::cout << "Did not find player: " << name << std::endl;

		f.close();
	}
	else
	{
		std::cerr << "Failed to load file: " << LeaderboardFilename << std::endl;
	}
	int ch = getchar();
}
