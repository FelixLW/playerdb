#include "Player.h"
#include "iostreamutils.h"
#include <string.h>
#include <iostream>

Player::Player(const char* _name, unsigned int highscore) 
{
	strcpy_s(this->name, MaxNameLength, _name);
	this->highScore = highscore;
}

void Player::Draw()
{
	std::cout << highScore << ", " << name << std::endl;
}

bool Player::LoadFromConsole()
{
	cinclear();

	std::cout << "Enter Player Name: ";
	std::cin >> name;
	std::cout << "Enter Player Score: ";
	std::cin >> highScore;

	if (highScore > MaxScore)
		highScore = MaxScore;

	return true;
}

bool Player::operator<(const Player& other) const
{
	return (highScore < other.highScore);
}